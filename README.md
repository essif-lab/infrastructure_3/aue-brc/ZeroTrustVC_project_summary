# ZeroTrustVC Project Summary

## Introduction
Enabling Zero Trust Architectures using OAuth2.0 and Verifiable Credentials 
(**ZeroTrustVC**) implements Authentication and Authorization for HTTP-based resources
using [JWT-encoded Verifiable Credentials](https://www.w3.org/TR/vc-data-model/#json-web-token).
The project is undertaken by the [Mobile Multimedia Laboratory](https://mm.aueb.gr/)
of the [Athens University of Economics and Business](https://www.aueb.gr).

ZeroTrustVC is based on the research paper [Capability-based access control 
for multi-tenant systems using OAuth 2.0 and Verifiable Credentials](https://mm.aueb.gr/publications/0a8b37c5-c814-4056-88a7-19556221728c.pdf) 
by N. Fotiou, V.A. Siris, G.C. Polyzos, appeared in 30th International Conference 
on Computer Communications and Networks (ICCCN). You can view a video presentation
of this paper [here](https://www.youtube.com/watch?v=B6biTo8w5zw)

ZeroTrustVC facilitates
capabilities-based access control, supports efficient VC revocation, and enables
"strong authentication and authorization of every access request" enabling resource
access over public, untrusted networks, aka Zero -Trust Architectures (ZTAs).

### Implementation
* [Python-based VC Verifier](https://github.com/mmlab-aueb/py-verifier)
* [.net VC Issuer based on OAuth2.0](https://github.com/mmlab-aueb/vc-issuer)

## Solution overview
The following figure gives a high level overview of ZeroTurstVC project.
![ZeroTurstVC overview](overview.png "ZeroTurstVC overview")

ZeroTrustVC considers users of an enterprise wishing to access an HTTP-based *protected resource*
using their *client* applications. Both clients and the protected resource may
be located in an network outside the administrative realm of the enterprise.

Users interact with an *authorization server*, owned by the enterprise using their client application. The authorization server maintains a *capability list* that maps a
user identifier to a list of capabilities. The user receives from the authorization
server a VC that includes the user identifier, as well as a list of the user's
capabilities.

ZeroTrustVC provides an HTTP proxy that intercepts the communication between
a client application and the protected resource. This HTTP proxy is configured
with a set of "rules" used for validating a VC. Client applications initiate a 
session with a protected resource (through the proxy) by including in the appropriate
HTTP header: (i) the received credential, and (ii) a *proof of possession*.
The HTTP proxy validates the VC based on the pre-configured rules and it verifies
the proof of possession. If all checks succeed the session is initiated and all
subsequent requests are forwarded to the protected resource. 

ZeroTrustVC also enables authorization servers to provide an efficient and privacy
preserving revocation mechanism. This revocation mechanism includes a compact
list of revoked VCs. At any point, any entity can verify the status of a VC. 

## Key properties
Compared to a conventional OAuth 2.0-based solution, ZeroTrustVC provides the
following advantages:

* The generated VCs are bound to the user identity, therefore it is safer to
store them for longer time (as opposed for example to mere "bearer tokens"), Hence,
client applications do not have to interact often with an authorization server.
* ZeroTrustVC uses VCs as "access tokens". VCs have richer semantics, can be used
for evaluating complex access control policies, and facilitate interoperability.
* ZeroTrustVC provides an efficient and privacy preserving mechanism for validating
the revocation status of an access token/VC

Compared to related VC-based solutions, ZeroTrustVC provides the following advantages:

* ZeroTrustVC builds on the widely used and well supported OAuth 2.0 flows for
managing the lifecycle of a VC.
* ZeroTrustVC leverages JWE for representing and protecting VCs. JWE is widely
used and standardized (as oppose for example to linked-data proofs).
* ZeroTrustVC uses JWT semantics to include a user identity (i.e., a public key)
directly into a VC. Hence,  VCs in ZeroTrustVC can be bound to a "subject" without
having to use "Decentralized Identifiers" (DIDs).